# muscio-app

#### 介绍
安卓课程作业

#### 软件架构
MVP
##### 成功获取网络数据
 ![img.png](img.png)
##### 完成首页和悦单界面的数据绑定
 ![img_1.png](img_1.png)
 ![img_2.png](img_2.png)
##### 实现mv播放
 ![img_4.png](img_4.png)
 ![img_3.png](img_3.png)
##### 完善mv播放页面
 ![img_5.png](img_5.png)
##### 实现音乐播放界面相关布局
 ![img_6.png](img_6.png)
##### 实现音乐播放进度获取，音乐名、作者名获取
 ![img_7.png](img_7.png)
##### 实现系统通知播放音乐
 ![img_8.png](img_8.png)
##### 主页面右上角设置界面（图标显示不出来，但可以点击，没找到原因）
 ![img_9.png](img_9.png)




