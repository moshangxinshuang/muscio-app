package com.example.myapplication.adapter

import android.content.Context
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.model.VideosBean
import com.example.myapplication.widget.MvItemView

class MvListAdapter:BaseListAdapter<VideosBean,MvItemView>() {
    override fun refreshItemView(itemView : MvItemView, data : VideosBean) {
        itemView.setData(data)
    }

    override fun getItemView(context : Context?) : MvItemView {
        return MvItemView(context)
    }
}