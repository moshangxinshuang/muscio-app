package com.example.myapplication.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.myapplication.model.MVTag
import com.example.myapplication.ui.fragement.MvPagerFragment

class MvPagerAdapter(val list : List<MVTag>, fm :FragmentManager?):
FragmentPagerAdapter(fm!!){

    override fun getItem(position : Int) : Fragment {
        //数据传递方式
        val fragment = MvPagerFragment()
        val bundle = Bundle()
        bundle.putString("args",list?.get(position)?.name)
        fragment.arguments = bundle
        return fragment
    }


    override fun getCount() : Int {
        return list?.size?:0//如果不为null返回list.size，否则返回0
    }

    override fun getPageTitle(position : Int) : CharSequence? {
        return list?.get(position)?.name//类别的名字
    }
}