package com.example.myapplication.adapter

import android.os.Parcel
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.myapplication.ui.fragement.DefaultFragment

class VideoPagerAdapter(fm:FragmentManager?):FragmentPagerAdapter(fm!!) {

    override fun getItem(position : Int) : Fragment {
        return DefaultFragment()
    }

    override fun getCount() : Int {
       return 3
    }
}