package com.example.myapplication.adapter

import android.content.Context
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.model.HistoryItemItemBean
import com.example.myapplication.widget.HistoryItemView


class HistoryAdapter:BaseListAdapter<HistoryItemItemBean,HistoryItemView>(){
    override fun refreshItemView(itemView : HistoryItemView, data : HistoryItemItemBean) {
        itemView.setData(data)
    }

    override fun getItemView(context : Context?) : HistoryItemView {
        return HistoryItemView(context)
    }

}