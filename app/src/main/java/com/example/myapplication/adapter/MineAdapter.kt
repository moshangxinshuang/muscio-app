package com.example.myapplication.adapter

import android.content.Context
import android.database.Cursor
import android.view.View
import android.view.ViewGroup
import androidx.cursoradapter.widget.CursorAdapter
import com.example.myapplication.model.AudioBean
import com.example.myapplication.widget.MineItemView

class MineAdapter(context : Context?,cursor:Cursor?):CursorAdapter(context,cursor) {
    override fun newView(context : Context?, cursor : Cursor?, parent : ViewGroup?) : View {
        return MineItemView(context)
    }

    override fun bindView(view : View?, context : Context?, cursor : Cursor?) {
        //
        val itemView = view as MineItemView
        //data
        val itemBean = AudioBean.getAudioBean(cursor)
        //
        itemView.setData(itemBean)
    }

}