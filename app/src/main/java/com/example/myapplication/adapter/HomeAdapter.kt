package com.example.myapplication.adapter
import android.content.Context
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.model.HomeItemBean
import com.example.myapplication.widget.HomeItemView

class HomeAdapter:BaseListAdapter<HomeItemBean,HomeItemView>() {
    override fun refreshItemView(itemView : HomeItemView, data : HomeItemBean) {
        itemView.setData(data)
    }

    override fun getItemView(context : Context?) : HomeItemView {
        return HomeItemView(context)
    }
}