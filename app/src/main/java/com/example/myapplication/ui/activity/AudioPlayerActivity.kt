package com.example.myapplication.ui.activity
import android.annotation.SuppressLint
import android.content.*
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.view.View
import android.widget.SeekBar
import com.example.myapplication.R
import com.example.myapplication.base.BaseActivity
import com.example.myapplication.model.AudioBean
import com.example.myapplication.service.AudioService
import com.example.myapplication.service.IService
import com.example.myapplication.utils.StringUtil.parseDuration
import kotlinx.android.synthetic.main.activity_music_player_bottom.*
import kotlinx.android.synthetic.main.activity_music_player_top.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class AudioPlayerActivity:BaseActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    val connection by lazy { AudioConnection() }
    var audioBean:AudioBean? = null
    var duration:Int = 0
    val handler = @SuppressLint("HandlerLeak")
    object :Handler(){
        @SuppressLint("HandlerLeak")
        override fun handleMessage(msg : Message) {
            when(msg.what){
                MSG_PROGRESS->startUpdateProgress()
            }
        }
    }
    val MSG_PROGRESS = 0


    override fun getLayoutId() : Int {
        return R.layout.activity_audio_player
    }

    override fun onClick(p0: View?){
        when(p0?.id){
            R.id.state->updatePlayState()
            R.id.pre->iService?.playPre()
            R.id.next->iService?.playNext()
        }
    }

    //接收端，接收eventbus方法,注意需要加上eventbus注解
    @Subscribe
    fun onEventMainThread(itemBean: AudioBean){
        //更新ui
        this.audioBean = itemBean
        //歌曲名，歌手名，播放界面顶端
        audio_title.text = itemBean.display_name
        artist.text = itemBean.artist
        //把播放状态改成正在播放
        updatePlayStateBtn()


        duration = iService?.getDuration()?:0
        //设置进度条最大值
        progress_sk.max = duration
        //更新播放进度
        startUpdateProgress()
    }

    private fun startUpdateProgress() {
        //获取当前播放进度，更新进度，定时获取新进度
       val progress:Int = iService?.getProgress()?:0
        updateProgress(progress)
        //当播放暂停时，也用该停止更新进度，这里可以优化
        handler.sendEmptyMessageDelayed(MSG_PROGRESS,1000)

    }

    @SuppressLint("SetTextI18n")
    private fun updateProgress(pro:Int) {
        //更新进度值
        progress.text = parseDuration(pro) +"/"+ parseDuration(duration)
        //更新进度条
        progress_sk.setProgress((pro))
    }

    private fun updatePlayState() {
        //更新播放状态，播放状态图标
        iService?.updatePlayState()
        updatePlayStateBtn()
    }

    private fun updatePlayStateBtn() {
        //根据当前播放状态更新图标
        val isplaying = iService?.isPlaying()
        isplaying?.let {
            if (isplaying){
                state.setImageResource(R.mipmap.btn_audio_play)
                //再次开启更新进度
                handler.sendEmptyMessage(MSG_PROGRESS)
            }else{
                state.setImageResource(R.mipmap.btn_audio_pause)
                //停止播放时也要停止更新进度
                handler.removeMessages(MSG_PROGRESS)
            }
        }
    }

    override fun initListeners() {
        //播放状态切换
        state.setOnClickListener(this)
        back.setOnClickListener { finish() }
        //进度条变化监听
        progress_sk.setOnSeekBarChangeListener(this)

        //上一曲、下一曲功能实现
        pre.setOnClickListener(this)
        next.setOnClickListener(this)

    }

    override fun initData() {
       // val list = intent.getParcelableArrayListExtra<AudioBean>("list")
        //val position = intent.getIntExtra("position",-1)
       //println("list=$list position=$position") 测试用
      //播放音乐
      /**val mediaPlayer = MediaPlayer()
        mediaPlayer.setOnPreparedListener{
            //准备好之后就开始播放
            mediaPlayer.start()
        }
      mediaPlayer.setDataSource(list?.get(position)?.data)
      mediaPlayer.prepareAsync()**/
        //开启service,通过intent将list和position传递,方法一
        //val intent = Intent(this,AudioService::class.java)
        //ist",list)
        //intent.putExtra("position",position)

        //注册eventbus
        EventBus.getDefault().register(this)

        //方法二
        val intent = intent
        intent.setClass(this,AudioService::class.java)
        //先开启时，iService是没有拿到数据的，所以再次点击同一首歌曲时，总时长更新不出来，所以要先绑定。这只是对于本项目有绑定开启的顺序之分。
        //绑定
        bindService(intent,connection, Context.BIND_AUTO_CREATE)
        //开启service
        startService(intent)
    }
    var iService:IService? = null//binder隐藏保护，与service的交互在接口Iservice中
   inner class AudioConnection:ServiceConnection{

        override fun onServiceConnected(p0 : ComponentName?, p1 : IBinder?) {
            iService = p1 as IService
        }

        //意外断开service时
        override fun onServiceDisconnected(p0 : ComponentName?) {

        }
    }
    override fun onDestroy() {
        //解绑服务
        unbindService(connection)
        //反注册eventbus,防止内存泄漏
        EventBus.getDefault().unregister(this)
        //清空handler发送的所有msg，防止内存泄漏，token传null值，所有的回调和msg会被移除。
        handler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

    //进度改变回调
    //p1,改变之后的进度
    //p2,为true(1)即代表通过用户手指拖动改变进度，为false代表通过代码改变，如 progress_sk.setProgress((pro))
    override fun onProgressChanged(p0 : SeekBar?, p1 : Int, p2 : Boolean) {
        if (!p2){
            return
        }else{
            //更新进度
            iService?.seekTo(p1)
            //更新进度显示
            updateProgress(p1)
        }
    }

    //手指触摸seekbar的回调
    override fun onStartTrackingTouch(p0 : SeekBar?) {

    }
    //手指离开seekbar回调
    override fun onStopTrackingTouch(p0 : SeekBar?) {

    }
}