package com.example.myapplication.ui.fragement

import android.content.Intent
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.example.myapplication.adapter.MvListAdapter
import com.example.myapplication.adapter.MvPagerAdapter
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.base.BaseListFragment
import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.model.MvPagerBean
import com.example.myapplication.model.VideoPlayBean
import com.example.myapplication.model.VideosBean
import com.example.myapplication.presenter.impl.MvListPresenterimpl
import com.example.myapplication.presenter.impl.MvPresenterimpl
import com.example.myapplication.ui.activity.VideoPlayerActivity
import com.example.myapplication.view.MvListView
import com.example.myapplication.widget.MvItemView
import org.jetbrains.anko.support.v4.startActivity

class MvPagerFragment :BaseListFragment<MvPagerBean,VideosBean,MvItemView>(),MvListView{
    var tinguid:String?=null

    override fun init() {
        tinguid = arguments?.getString("args")
    }

    override fun getSpecialAdapter() : BaseListAdapter<VideosBean, MvItemView> {
        return MvListAdapter()
    }

    override fun getSpecialPresenter() : BaseListPresenter {
        return MvListPresenterimpl(tinguid!!,this)
    }

    override fun getList(response : MvPagerBean?) : List<VideosBean>? {
        return response?.data
    }

    override fun initListeners() {
        super.initListeners()
        adapter.setMyListener {
            //println("it=$it")
            //startActivity<VideoPlayerActivity>("item" to it)
            val intent = Intent(context,VideoPlayerActivity::class.java)
            intent.putExtra(
                "item",
                VideoPlayBean(
                    it.id,
                    it.name,
                    "https://videocdn.taobao.com/oss/ali-video/d6bc4ae3eb3c866bee9903d47d1210c6/video.mp4"
                )
            )
            startActivity(intent)
        }
    }
}