package com.example.myapplication.ui.fragement

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapter.HistoryAdapter
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.base.BaseListFragment
import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.model.HistoryBean
import com.example.myapplication.model.HistoryItemItemBean
import com.example.myapplication.presenter.impl.HistoryPresentrimpl
import com.example.myapplication.view.HistoryView
import com.example.myapplication.widget.HistoryItemView
import kotlinx.android.synthetic.main.fragment_home.*
import java.lang.ref.WeakReference

class HistoryFragment:BaseListFragment<HistoryBean,HistoryItemItemBean,HistoryItemView>() {
    override fun getSpecialAdapter() : BaseListAdapter<HistoryItemItemBean, HistoryItemView> {
        return HistoryAdapter()
    }

    override fun getSpecialPresenter() : BaseListPresenter {
       return HistoryPresentrimpl(WeakReference(this))
    }

    override fun getList(response : HistoryBean?) : List<HistoryItemItemBean>? {
        return response?.songlist
    }
}