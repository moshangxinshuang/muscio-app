package com.example.myapplication.ui.fragement

import android.graphics.Color
import android.view.View
import android.webkit.WebSettings
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapter.HomeAdapter
import com.example.myapplication.application.MyApplication
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.base.BaseListAdapter
import com.example.myapplication.base.BaseListFragment
import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.model.HomeBean
import com.example.myapplication.model.HomeItemBean
import com.example.myapplication.presenter.impl.HomePresenterimpl
import com.example.myapplication.utils.ThreadUtil
import com.example.myapplication.utils.URLProviderUtils
import com.example.myapplication.view.HomeView
import com.example.myapplication.widget.HomeItemView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*
import okhttp3.*
import java.io.IOException
import java.lang.ref.WeakReference
import java.time.ZoneOffset

class HomeFragment:BaseListFragment<HomeBean,HomeItemBean,HomeItemView>(){
    override fun getSpecialAdapter() : BaseListAdapter<HomeItemBean, HomeItemView> {
        return HomeAdapter()
    }

    override fun getSpecialPresenter() : BaseListPresenter {
        return HomePresenterimpl(WeakReference(this))
    }

    override fun getList(response : HomeBean?) : List<HomeItemBean>? {
        return response?.songlist
    }

}