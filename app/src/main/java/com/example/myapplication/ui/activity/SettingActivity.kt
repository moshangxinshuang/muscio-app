package com.example.myapplication.ui.activity

import androidx.appcompat.widget.Toolbar
import com.example.myapplication.R
import com.example.myapplication.base.BaseActivity
import com.example.myapplication.utils.ToolBarManager
import org.jetbrains.anko.find

class SettingActivity:BaseActivity(),ToolBarManager {
    override fun getLayoutId() : Int {
        return R.layout.activity_settings
    }

    override val toolbar by lazy {
        find<Toolbar>(R.id.toolbar)
    }

    override fun initData() {
        super.initData()
        initSettingToolBar()
    }
}