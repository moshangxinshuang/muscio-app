package com.example.myapplication.ui.activity

import com.example.myapplication.R
import com.example.myapplication.base.BaseActivity

class AboutActivity:BaseActivity() {
    override fun getLayoutId() : Int {
        return R.layout.activity_about
    }
}