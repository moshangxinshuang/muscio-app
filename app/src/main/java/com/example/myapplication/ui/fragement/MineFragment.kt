package com.example.myapplication.ui.fragement

import android.Manifest
import android.content.AsyncQueryHandler
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.myapplication.R
import com.example.myapplication.adapter.MineAdapter
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.model.AudioBean
import com.example.myapplication.ui.activity.AudioPlayerActivity
import com.example.myapplication.utils.CursorUtil
import kotlinx.android.synthetic.main.fragment_mine.*
import org.jetbrains.anko.support.v4.startActivity
import java.util.ArrayList

class MineFragment:BaseFragment() {
    var adapter:MineAdapter? =null

    override fun initView() : View? {
        return View.inflate(context, R.layout.fragment_mine,null)
    }

    override fun initListeners() {
        super.initListeners()
        adapter = MineAdapter(context,null)
        listView.adapter = adapter
        //设置条目点击事件
        listView.setOnItemClickListener{ adapterView,view,i,l ->
            //获取音乐数据条目集合
            val cursor = adapter?.getItem(i) as Cursor
            //当前cursor
            val list:ArrayList<AudioBean> = AudioBean.getAudioBeans(cursor)
            //跳转音乐播放界面
            startActivity<AudioPlayerActivity>("list" to list,"position" to i)
        }
    }

    override fun initData() {
        super.initData()
        //动态权限申请
        handlePermission()
    }

    private fun handlePermission(){
        val permission = Manifest.permission.READ_EXTERNAL_STORAGE
        //查看是否具有相应权限
        val checkPermission = context?.let { ActivityCompat.checkSelfPermission(it,permission) }
        if (checkPermission == PackageManager.PERMISSION_GRANTED){
            loadsongs()
        }else{
            activity?.let {
                if (ActivityCompat.shouldShowRequestPermissionRationale(it,permission)){
                    //弹出对话框询问，获取相应权限
                    AlertDialog.Builder(it)
                        .setTitle("请确认")
                        .setMessage("当前应用正尝试获取高危权限！")
                        .setPositiveButton("确定"){d,w->
                            myRequestPermission()
                        }
                        .setNegativeButton("取消"){d,w->
                            Toast.makeText(it,"已取消",Toast.LENGTH_LONG).show()
                        }
                        .show()
                }else{
                    myRequestPermission()
                }
            }
        }
    }

    private fun myRequestPermission(){
        val permission = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        requestPermissions(permission,1)
    }

    private fun loadsongs(){
        val resolver = context?.contentResolver
        val handler = object :AsyncQueryHandler(resolver){
            override fun onQueryComplete(token : Int, cookie : Any?, cursor : Cursor?) {
                //刷新adapter
                (cookie as MineAdapter).swapCursor(cursor)
            }
        }
        //开始查询
        handler.startQuery(
            0,adapter,MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,

            arrayOf(
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST
            ),
            null,null,null
        )
    }

    override fun onRequestPermissionsResult(
        requestCode : Int,
        permissions : Array<out String>,
        grantResults : IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
            loadsongs()
        }
    }

    override fun onDestroy() {
        //释放cursor，避免内存泄露
        super.onDestroy()
        adapter?.changeCursor(null)
    }
}