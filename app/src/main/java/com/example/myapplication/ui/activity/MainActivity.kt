package com.example.myapplication.ui.activity

import androidx.appcompat.widget.Toolbar
import com.example.myapplication.R
import com.example.myapplication.base.BaseActivity
import com.example.myapplication.utils.FragmentUtil
import com.example.myapplication.utils.ToolBarManager
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find


class MainActivity :BaseActivity(),ToolBarManager {
    override fun getLayoutId() : Int {
        return R.layout.activity_main
    }

    override val toolbar by lazy {
        find<Toolbar>(R.id.toolbar)
    }//val 的延迟初始化

    override fun initData() {
        super.initData()
        initMainToolBar()
    }

    //bottombar的监听处理事件
    override fun initListeners() {//要在build.app中添加相应插件 id 'kotlin-android-extensions'
        super.initListeners()
        bottomBar.setOnTabSelectListener{
           // println("aaaaaaaajjjj")//测试用，之前一直没有对应的fragment界面出现，经过测试发现，是container太小了，显示了文字自己根本看不到！！！
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container,FragmentUtil.fragmentUtil.getFragment(it)!!,it.toString())
            transaction.commit()
        }
    }
}