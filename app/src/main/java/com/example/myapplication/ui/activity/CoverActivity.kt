package com.example.myapplication.ui.activity

import android.view.View
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorListener
import com.example.myapplication.R
import com.example.myapplication.base.BaseActivity

class CoverActivity :BaseActivity(),ViewPropertyAnimatorListener{


    override fun getLayoutId() : Int {
        return R.layout.activity_cover
    }

    override fun initData() {
        val Cover1 = findViewById<ImageView>(R.id.imageView)
        super.initData()
        ViewCompat.animate(Cover1).scaleX(1.0f).scaleY(1.0f).setListener(this).setStartDelay(1000).setDuration(2500)//软件封面动画展示
    }

    override fun onAnimationStart(view : View?) {
        //进入主界面
       startActivityAndFinish<MainActivity>()
    }

    override fun onAnimationEnd(view : View?) {

    }

    override fun onAnimationCancel(view : View?) {

    }

}