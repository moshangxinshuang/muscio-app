package com.example.myapplication.ui.fragement

import android.view.View
import com.example.myapplication.R
import com.example.myapplication.adapter.MvPagerAdapter
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.model.MVTag
import com.example.myapplication.presenter.impl.MvPresenterimpl
import com.example.myapplication.view.MvView
import kotlinx.android.synthetic.main.fragment_mv.*

class MvFragment:BaseFragment() ,MvView{
    val presenter by lazy { MvPresenterimpl(this) }

    override fun initView(): View? {
        return View.inflate(context, R.layout.fragment_mv,null)
    }

    override fun initData() {
        //加载区域数据
        presenter.loadDatas()
    }


    override fun onError(msg : String?) {
        showToast("加载区域数据失败：${msg}")
    }

    override fun onSuccess(result : List<MVTag>) {
        //showToast("加载区域数据成功")
        //在fragment中显示fragment需要用childrenFragmentManager
        val adapter = MvPagerAdapter(result,childFragmentManager)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }
}