package com.example.myapplication.ui.activity

import androidx.viewpager.widget.ViewPager
import cn.jzvd.Jzvd
import com.example.myapplication.R
import com.example.myapplication.adapter.VideoPagerAdapter
import com.example.myapplication.base.BaseActivity
import com.example.myapplication.model.VideoPlayBean
import com.example.myapplication.utils.FileUtil
import kotlinx.android.synthetic.main.activity_video_player.*

class VideoPlayerActivity:BaseActivity (){
    override fun getLayoutId() : Int {
        return R.layout.activity_video_player
    }

    override fun initData() {
        super.initData()
        val data = intent.data
        println("data=$data")
        if (data == null){//应用视频内处理
            val videoPlayBean = intent.getParcelableExtra<VideoPlayBean>("item")
            jz_video.setUp(
                videoPlayBean?.url,
                videoPlayBean?.title
            )
        }else{//别的应用用本应用打开网络视频
            if (data.toString().startsWith("http")){
                jz_video.setUp(
                    data.toString(),data.toString()
                )
            }

            //别的应用用本应用打开本地视频
            val filePath = FileUtil.getFileFromUri(data,this)?.absolutePath
            jz_video.setUp(
                filePath,filePath
            )
        }
    }

    override fun onBackPressed() {
        if (Jzvd.backPress()){
            return
        }
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        Jzvd.releaseAllVideos()
    }

    override fun initListeners() {
        //适配adapter
        viewPager.adapter = VideoPagerAdapter(supportFragmentManager)
        //选中监听
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            //滑动状态改变的回调
            override fun onPageScrollStateChanged(state: Int) {
            }
            //滑动回调
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }
            //选中状态改变回调
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> rg.check(R.id.rb1)
                    1 -> rg.check(R.id.rb2)
                    2 -> rg.check(R.id.rb3)
                }
            }
        })
    }
}