package com.example.myapplication.ui.fragement

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.example.myapplication.base.BaseFragment

class DefaultFragment:BaseFragment() {
    override fun initView() : View? {
        val textView = TextView(context)
        textView.gravity = Gravity.CENTER
        textView.setTextColor(Color.BLUE)
        textView.text = "暂无相关信息"
        return textView
    }
}