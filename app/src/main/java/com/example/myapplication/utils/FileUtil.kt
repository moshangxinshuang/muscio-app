package com.example.myapplication.utils

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.text.TextUtils
import java.io.*

object FileUtil {
    fun getFileFromUri(uri:Uri?,context : Context?):File?{
        return if (uri == null) {
            null
        } else when (uri.getScheme()) {
            "content" -> getFileFromContentUri(uri, context)
            "file" -> File(uri.getPath())
            else -> null
        }
    }

    @SuppressLint("Range")
    private fun getFileFromContentUri(contentUri: Uri?, context: Context?): File? {
        if (contentUri == null) {
            return null
        }
        var file: File? = null
        var filePath: String? = null
        val fileName: String
        val filePathColumn =
            arrayOf(MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME)
        val contentResolver: ContentResolver? = context?.getContentResolver()
        val cursor: Cursor? = contentResolver?.query(
            contentUri, filePathColumn, null,
            null, null
        )
        if (cursor != null) {
            cursor.moveToFirst()
            try {
                filePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]))
            } catch (e: Exception) {
            }
            fileName = cursor.getString(cursor.getColumnIndex(filePathColumn[1]))
            cursor.close()
            if (!TextUtils.isEmpty(filePath)) {
                file = File(filePath)
            }
            if (!file!!.exists() || file.length() <= 0 || TextUtils.isEmpty(filePath)) {
                filePath = getPathFromInputStreamUri(context, contentUri, fileName)
            }
            if (!TextUtils.isEmpty(filePath)) {
                file = File(filePath)
            }
        }
        return file
    }

    //用流拷贝该视频文件到自己的app目录下
    fun getPathFromInputStreamUri(context: Context?, uri: Uri, fileName: String): String? {
        var inputStream: InputStream? = null
        var filePath: String? = null
        if (uri.authority != null) {
            try {
                inputStream = context?.contentResolver?.openInputStream(uri)
                val file = createTemporalFileFrom(context, inputStream, fileName)
                filePath = file!!.path
            } catch (e: java.lang.Exception) {
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close()
                    }
                } catch (e: java.lang.Exception) {
                }
            }
        }
        return filePath
    }

    @Throws(IOException::class)
    private fun createTemporalFileFrom(
        context: Context?,
        inputStream: InputStream?,
        fileName: String
    ): File? {
        var targetFile: File? = null
        if (inputStream != null) {
            var read: Int
            val buffer = ByteArray(8 * 1024)
            //定义拷贝的文件路径
            targetFile = File(context?.getCacheDir(), fileName)
            if (targetFile.exists()) {
                targetFile.delete()
            }
            val outputStream: OutputStream = FileOutputStream(targetFile)
            while (inputStream.read(buffer).also { read = it } != -1) {
                outputStream.write(buffer, 0, read)
            }
            outputStream.flush()
            try {
                outputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return targetFile
    }
}