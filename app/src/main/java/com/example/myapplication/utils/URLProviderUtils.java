package com.example.myapplication.utils;

import static android.os.Build.HOST;

import android.util.Log;

public class URLProviderUtils {
    //获取首页的url

    public static  String getHomeUrl(int offset,int size){
        String url = "http://cloud-music.pl-fe.cn/mv/all?area=%E6%B8%AF%E5%8F%B0"
                + "&offset=" + offset
                + "&limits=" + size;
        Log.i("Main_url", url);
        return url;
    }
    public static  String getHistoryUrl(int offset,int size){
        String url = "http://cloud-music.pl-fe.cn/mv/all?area=%E6%B8%AF%E5%8F%B0"
                + "&offset=" + offset
                + "&limits=" + size;
        Log.i("Main_url", url);
        return url;
    }
    //获取mv列表的url

    public static String getMvAreaUrl(){
        String url = "http://cloud-music.pl-fe.cn/playlist/highquality/tags";
        Log.i("Main_url",url);
        return url;
    }

    public  static String getMvListUrl(String area,int offset,int size){
        String url = "http://cloud-music.pl-fe.cn/mv/all?area="+area
                +"&offset="+offset
                +"&limit="+size;
        Log.i("Main_url",url);
        return url;
    }
}
