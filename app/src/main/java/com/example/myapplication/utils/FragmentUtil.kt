package com.example.myapplication.utils

import com.example.myapplication.R
import com.example.myapplication.base.BaseFragment
import com.example.myapplication.ui.fragement.HistoryFragment
import com.example.myapplication.ui.fragement.HomeFragment
import com.example.myapplication.ui.fragement.MineFragment
import com.example.myapplication.ui.fragement.MvFragment

//管理fragment的util类
class FragmentUtil private constructor() {

    val homeFragment by lazy { HomeFragment() }
    val mvFragment by lazy { MvFragment() }
    val mineFragment by lazy { MineFragment() }
    val historyFragment by lazy { HistoryFragment() }

    companion object {//懒加载，有利于线程安全。
        val fragmentUtil by lazy { FragmentUtil() }
    }

    fun getFragment(tabId : Int) : BaseFragment ?{//根据tabid生成对应fragment
         when (tabId) {
            R.id.tab_home -> return homeFragment
            R.id.tab_mv -> return mvFragment
            R.id.tab_mine -> return mineFragment
            R.id.tab_history -> return historyFragment
        }
        return null
    }
}