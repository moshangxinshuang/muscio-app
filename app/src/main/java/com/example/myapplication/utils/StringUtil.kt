package com.example.myapplication.utils

//处理时间转换
object StringUtil {
    val HOUR = 60*60*1000
    val MIN = 60*1000
    val SEC = 1000
    fun parseDuration(progress:Int):String{
        val hour = progress/ HOUR
        val min = progress% HOUR/ MIN
        val sec = progress% MIN/ SEC
        var result : String = if (hour == 0){
            //不足一小时不显示HOUR
            String.format("%02d:%02d",min,sec)//将当前min，sec转化成十进制，不足两位则以0补齐
        }else{
            String.format("%02d:%02d%02d",hour,min,sec)
        }
        return result
    }
}