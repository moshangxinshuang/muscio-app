package com.example.myapplication.utils

import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.example.myapplication.R
import com.example.myapplication.ui.activity.SettingActivity

//标题栏封装管理类
interface ToolBarManager {//初始化交由子类来提供，声明为接口，val变量就不再需要强制初始化。
    val toolbar: Toolbar
    //主界面的toolbar
    fun initMainToolBar(){
        println("lazy initMainToolBar") //测试用
        toolbar.run {
            title = "影音播放器"
            inflateMenu(R.menu.main)
        }
        toolbar.setOnMenuItemClickListener{
            when(it?.itemId){//Kotlin调用Java，“如果Java接口中只有一个未实现的方法，可以省略接口对象，直接用{}表示未实现的方法”
               // R.id.setting->Toast.makeText(toolbar.context,"menu clicked",Toast.LENGTH_SHORT).show()
                R.id.setting->toolbar.context.startActivity(
                    Intent(
                        toolbar.context,
                        SettingActivity::class.java
                    )
                )
            }
            true
        }
    }
    //设置界面toolbar
    fun initSettingToolBar(){
        toolbar.setTitle("设置界面")
    }
}