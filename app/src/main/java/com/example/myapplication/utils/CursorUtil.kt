package com.example.myapplication.utils

import android.database.Cursor

object CursorUtil {
    fun lonCursor(cursor : Cursor?){
        cursor?.let {
            //将cursor游标复位
            it.moveToPosition(-1)
            //查询打印歌曲名称
            while (it.moveToNext()){
                for (index in 0 until it.columnCount){
                    println("key=${it.getColumnName(index)} --values=${it.getString(index)}")
                }
            }
        }
    }
}