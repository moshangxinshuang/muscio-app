package com.example.myapplication.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.toast


//Fragment基类
abstract class BaseFragment:Fragment() {
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }
    //fragment初始化
    open protected fun init(){

    }

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View? {
        return initView()
    }

    override fun onActivityCreated(savedInstanceState : Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initListeners()
        initData()
    }

    open protected fun initData(){

    }

    open protected fun initListeners(){

    }

    open protected fun showToast(msg:String){
        context?.runOnUiThread { toast(msg) }
    }
    protected abstract fun initView():View?


}