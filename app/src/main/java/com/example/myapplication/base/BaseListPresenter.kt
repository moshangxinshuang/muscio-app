package com.example.myapplication.base

interface BaseListPresenter {
    companion object{
        val TYPE_INIR_OR_REFRESH =1
        val TYPE_LOAD_MORE =2
    }

    fun loadDatas()
    fun loadMoreDatas(offset : Int)
    fun destoryView()
}