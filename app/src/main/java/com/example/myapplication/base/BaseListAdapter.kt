package com.example.myapplication.base

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.widget.LoadMoreView

abstract class BaseListAdapter<ITEMBEAN,ITEMVIEW:View> :
    RecyclerView.Adapter<BaseListAdapter.BaseListHolder>(){
    private var list = ArrayList<ITEMBEAN>()

    //定义函数类型变量
    var listener:((itemBean:ITEMBEAN)-> Unit)? = null
    //定义回调方法
    fun setMyListener(listener:(itemBean:ITEMBEAN)->Unit){
        this.listener = listener
    }

    fun setData(list : List<ITEMBEAN>?){
        list?.let {
            //只在list不空时执行
            this.list.clear()
            this.list.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun loadMoreData(list : List<ITEMBEAN>?){
        list?.let {
            this.list.addAll(list)
            notifyDataSetChanged()
        }
    }


    class BaseListHolder(itemView : View): RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : BaseListHolder {
        if(viewType ==1){
            return BaseListHolder(LoadMoreView(parent?.context))
        }else{
            return BaseListHolder(getItemView(parent?.context))
        }
    }

    override fun getItemViewType(position : Int) : Int {
        if(position ==list.size){//上拉如果是最后一条则加载更多
            return 1
        }else{
            return 0
        }
    }

    override fun getItemCount() : Int {
        return list.size+1
    }

    override fun onBindViewHolder(holder : BaseListHolder, position : Int) {
        if (position == list.size) return
        val data =list.get(position)
        val itemView =holder.itemView as ITEMVIEW
        refreshItemView(itemView,data)
        //设置条目的点击事件
        itemView.setOnClickListener {
            listener?.let {
                it(data)
            }
            //也可以这样写：
            //listener?.invoke(data)
        }
    }

    //刷新条目view
    abstract fun refreshItemView(itemView : ITEMVIEW, data : ITEMBEAN)

    //获取条目view
    abstract fun getItemView(context:Context?):ITEMVIEW
}