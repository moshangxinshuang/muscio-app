package com.example.myapplication.base

import com.example.myapplication.model.HomeBean
//抽象方法抽取
interface BaseView <RESPONSE>{
    //获取数据失败
    fun onError(message:String?)
   // 初始化数据或刷新数据成功
    fun loadSuccessed(list : RESPONSE?)
    //加载更多成功
    fun onLoadMore(list : RESPONSE?)
}