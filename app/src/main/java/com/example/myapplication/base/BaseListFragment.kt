package com.example.myapplication.base

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.fragment_home.*

abstract class BaseListFragment<RESPONSE,ITEMBEAN,ITEMVIEW:View>:BaseFragment(),BaseView<RESPONSE> {
    val adapter by lazy { getSpecialAdapter() }//局部变量

    val presenter by lazy { getSpecialPresenter() }

    //获取适配器
    abstract fun getSpecialAdapter():BaseListAdapter<ITEMBEAN,ITEMVIEW>

    //获取presenter
    abstract fun getSpecialPresenter():BaseListPresenter

    //获取列表数据集合
    abstract fun getList(response : RESPONSE?) : List<ITEMBEAN>?

    override fun initView() : View {
        return View.inflate(context, R.layout.fragment_home,null)
    }

    override fun initListeners() {
        super.initListeners()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        //监听列表滑动
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView : RecyclerView, newState : Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    val layoutManager = recyclerView.layoutManager
                    if (layoutManager is LinearLayoutManager){
                        val manager: LinearLayoutManager = layoutManager
                        val lastPosition = manager.findLastVisibleItemPosition()
                        if (lastPosition == adapter.itemCount-1){//开始加载更多数据
                            presenter.loadMoreDatas(adapter.itemCount-1)
                        }
                    }
                }
            }
        })
        lay_refresh.setColorSchemeColors(Color.BLUE, Color.GRAY, Color.YELLOW)
        lay_refresh.setOnRefreshListener {
            presenter.loadDatas()
        }
    }

    //请求网络数据

    override fun initData() {
        super.initData()
       presenter.loadDatas()
    }

    override fun onError(message : String?) {
        showToast("获取数据出错")
        lay_refresh.isRefreshing = false
    }

    override fun loadSuccessed(response : RESPONSE?) {
        lay_refresh.isRefreshing = false
        adapter.setData(getList(response))
    }

    override fun onLoadMore(response : RESPONSE?) {
        lay_refresh.isRefreshing = false
        adapter.loadMoreData(getList(response))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter?.destoryView()
    }
}