package com.example.myapplication.base

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

//activity抽象基类
abstract class BaseActivity:AppCompatActivity() {


    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initListeners()
        initData()
    }
    //open关键字，便于子类覆写
    open protected fun initData(){}
    open protected fun initListeners(){}

    open protected fun showToast(msg:String){//封装Toast，方便线程处理
        runOnUiThread{toast(msg)}
    }

    inline fun <reified T:Activity> startActivityAndFinish(){//封装activity的开始和结束，便于调用。
        startActivity<T>()//通过泛型来获取到相应的class，refiled修饰来变成内联函数
        finish()
    }

    protected abstract fun getLayoutId():Int
}