package com.example.myapplication.view

import com.example.myapplication.base.BaseView
import com.example.myapplication.model.MvPagerBean

interface MvListView:BaseView<MvPagerBean> {
}