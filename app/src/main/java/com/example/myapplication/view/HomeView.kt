package com.example.myapplication.view

import com.example.myapplication.base.BaseView
import com.example.myapplication.model.HomeBean

interface HomeView :BaseView<HomeBean>{
    //继承抽象类方法，可以自行扩展
}