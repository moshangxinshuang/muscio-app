package com.example.myapplication.view

import com.example.myapplication.model.MVTag

interface MvView {
    fun onError(msg:String?)
    fun onSuccess(result : List<MVTag>)
}