package com.example.myapplication.view

import com.example.myapplication.base.BaseView
import com.example.myapplication.model.HistoryBean


interface HistoryView :BaseView<HistoryBean>{
    //继承抽象类方法，可以自行扩展
}