package com.example.myapplication.net

import com.example.myapplication.model.HomeBean
import com.example.myapplication.utils.URLProviderUtils


class HomeRequest(type:Int,offset : Int,responseHandler : ResponseHandler<HomeBean>):
MediaRequest<HomeBean>(type,URLProviderUtils.getHomeUrl(offset,5),responseHandler){

}