package com.example.myapplication.net

import android.webkit.WebSettings
import com.example.myapplication.application.MyApplication
import com.example.myapplication.utils.ThreadUtil
import okhttp3.*
import java.io.IOException

//发送网络请求的管理类

class NetManger private constructor(){
    private val okHttpClient by lazy { OkHttpClient() }//OkhttpClient对象不需要每次都创建，可以定义成全局变量，用懒加载即可。

    companion object{//单例类
        val netManger by lazy { NetManger() }
    }

    fun <RESPONSE> sendRequest(req:MediaRequest<RESPONSE>){
        val request = Request.Builder()
            .get()
            .url(req.url)
            .addHeader("User-Agent", WebSettings.getDefaultUserAgent(MyApplication.instance))
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {//kotlin用object来进行回调处理。写完逻辑后应该要去manifest添加网络权限
        override fun onFailure(call : Call, e : IOException) {
                req.handler.onError(req.type,e?.message)
            }

            override fun onResponse(call : Call, response : Response) {
                val result = response?.body?.string()
                /* println(result)//测试用 */
                val parseResult = req.parseResult(result)
                ThreadUtil.runOnMainThread { req.handler.onSuccessed(req.type, parseResult) }
            }
        })
    }
}