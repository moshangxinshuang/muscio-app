package com.example.myapplication.net

import com.example.myapplication.model.MvAreaBean
import com.example.myapplication.utils.URLProviderUtils

class MvAreaRequest(handler : ResponseHandler<MvAreaBean>):
MediaRequest<MvAreaBean>(0,URLProviderUtils.getMvAreaUrl(),handler){
}