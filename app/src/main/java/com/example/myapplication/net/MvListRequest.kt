package com.example.myapplication.net

import com.example.myapplication.model.MvPagerBean
import com.example.myapplication.utils.URLProviderUtils

class MvListRequest(type : Int,code:String,offset:Int,handler : ResponseHandler<MvPagerBean>):
MediaRequest<MvPagerBean>(type,URLProviderUtils.getMvListUrl(code,offset,30),handler) {

}