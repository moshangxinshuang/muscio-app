package com.example.myapplication.net

import com.example.myapplication.model.HistoryBean
import com.example.myapplication.utils.URLProviderUtils
import com.example.myapplication.view.HistoryView
import java.time.ZoneOffset

class HistoryRequest(type:Int,offset : Int,handler : ResponseHandler<HistoryBean>):
MediaRequest<HistoryBean>(type,URLProviderUtils.getHistoryUrl(offset,20),handler){
}