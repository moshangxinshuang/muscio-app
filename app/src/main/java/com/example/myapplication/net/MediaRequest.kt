package com.example.myapplication.net

import com.google.gson.Gson
import java.lang.reflect.ParameterizedType

//网络请求

open class MediaRequest <RESPONSE>(
    val type:Int,//增加一个类型，来决定是调用loadMore还是loadSuccessed
    val url :String,
    val handler : ResponseHandler<RESPONSE>){
    //解析网络请求结果
    fun parseResult(result : String?):RESPONSE{
        val gson = Gson()
        //获取泛型类型
        val type = (this.javaClass.genericSuperclass as ParameterizedType).getActualTypeArguments()[0]
        val list = gson.fromJson<RESPONSE>(result,type)
        return list
    }

    //发送网络请求
    fun excute(){
        NetManger.netManger.sendRequest(this)
    }
}