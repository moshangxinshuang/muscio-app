package com.example.myapplication.service

interface IService {
    fun updatePlayState()
    fun isPlaying():Boolean?
    fun getDuration() : Int
    fun getProgress() : Int
    fun seekTo(p1 : Int)
    fun playPre()
    fun playNext()
}