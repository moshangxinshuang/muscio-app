package com.example.myapplication.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.example.myapplication.R
import com.example.myapplication.model.AudioBean
import com.example.myapplication.ui.activity.AudioPlayerActivity
import com.example.myapplication.ui.activity.MainActivity
import org.greenrobot.eventbus.EventBus
import java.util.ArrayList


class AudioService: Service() {
    var list:ArrayList<AudioBean>? = null
    var position :Int? = -2 //避免和pos数值一样，造成播放第一首歌时闪退
    var mediaPlayer:MediaPlayer? = null
    var notification :Notification? = null
    var manager:NotificationManager? = null
    val binder by lazy { AudioBinder() }

    val FROM_PRE =1
    val FROM_NEXT =2
    val FROM_STATE =3
    val FROM_CONTENT =4

    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent : Intent?, flags : Int, startId : Int) : Int {
        //service进程优先级比较高，意外杀死之后会尝试重新启动，很有可能导致空指针异常
        //START_STICK ，service被意外杀死后，会尝试重新启动service，不会传递原来的intent，会导致null
        //START_NOT_STICK，service被意外杀死后，不会尝试重新启动service
        //START_REDELIVER_INTENT，service被意外杀死后，会尝试重新启动service，会传递原来的intent。
        //获取数据集合和当前position

        //判断进入service方法
        val from = intent?.getIntExtra("from",-1)
        when(from){
            FROM_PRE->{binder.playPre()}
            FROM_NEXT->{binder.playNext()}
            FROM_CONTENT->{//进入播放器，通知service更新界面
                binder.notifyUpdateUi()
            }
            FROM_STATE->{
                binder.updatePlayState()
            }
            else->{
                val pos = intent?.getIntExtra("position",-1)?:-1
                //开始播放音乐
                if (pos!=position) {//如果点击的不是刚才播放的歌曲，就需要重新开始播放
                    position = pos
                    list = intent?.getParcelableArrayListExtra<AudioBean>("list")
                    binder.playItem()
                }else{
                    //如果还是刚才播放的音乐，主动通知界面更新
                    binder.notifyUpdateUi()
                }
            }
        }
        return START_NOT_STICKY
    }

    override fun onBind(p0 : Intent?) : IBinder? {
        return binder
    }

   inner class AudioBinder:Binder(),IService, MediaPlayer.OnPreparedListener,
       MediaPlayer.OnCompletionListener {//内部类，访问内部数据

       override fun onPrepared(p0 : MediaPlayer?) {
           //播放音乐！
           mediaPlayer?.start()
           //通知界面进行更新
           notifyUpdateUi()
           //显示通知
           showNotificatiion()
       }

       fun playItem(){
           //因为播放上一曲和下一曲都开了新的mediaplayer，所以要判断有没有多余的mediaplayer
           if (mediaPlayer!=null){//将多余mediaplayer释放
               mediaPlayer?.reset()
               mediaPlayer?.release()
               mediaPlayer = null
           }
           mediaPlayer = MediaPlayer()
           mediaPlayer?.let {
               it.setOnPreparedListener(this)
               it.setOnCompletionListener(this)
               it.setDataSource(list?.get(position!!)?.data)
               it.prepareAsync()
           }
        }
       fun notifyUpdateUi() {
           //通知界面更新,发送端
           EventBus.getDefault().post(list?.get(position!!))
       }

       override fun updatePlayState() {
           //更新播放状态,先获取当前播放状态
           val isplaying = isPlaying()

           isplaying?.let {
               if (isplaying){//暂停播放
                  pause()
               }else{//开始播放
                   start()
               }
           }
       }

       //暂停播放
       private fun pause(){
           mediaPlayer?.pause()
           EventBus.getDefault().post(list?.get(position!!))
           //更新通知栏图标
           notification?.contentView?.setImageViewResource(R.id.state,R.mipmap.btn_audio_pause_m)
           //重新显示
           manager?.notify(1,notification)
       }

       //开始播放
       private fun start(){
           mediaPlayer?.start()
           EventBus.getDefault().post(list?.get(position!!))
           //更新通知栏图标
           notification?.contentView?.setImageViewResource(R.id.state,R.mipmap.btn_audio_play_m)
           //重新显示
           manager?.notify(1,notification)
       }

       override fun isPlaying():Boolean?{
           return mediaPlayer?.isPlaying
       }

       override fun getDuration() : Int {
           return mediaPlayer?.duration?:0
       }

       override fun getProgress() : Int {
           return mediaPlayer?.currentPosition?:0
       }

       override fun seekTo(p1 : Int) {
           //跳转到当前进度
           mediaPlayer?.seekTo(p1)
       }

       override fun playPre() {
           //播放上一曲
           //获取position
           list?.let {position = (position?.dec())?.rem(it.size) }
           playItem()
       }

       override fun playNext() {
           //播放下一曲
           autoPlayNext()
       }

       override fun onCompletion(p0 : MediaPlayer?) {
           autoPlayNext()
       }

       private fun autoPlayNext() {
           //自动播放下一曲
           list?.let {position = (position?.plus(1))?.rem(it.size) }
           playItem()
       }

       //显示通知
       private fun showNotificatiion() {
           manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
               val channelId = "default"
               val channelName = "默认通知"
               manager?.createNotificationChannel(NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH))
           }
           notification = getNotification()
           manager?.notify(1,notification)
       }

       //之前无法显示通知的原因是：
       //每次进行通知触发时，对系统版本进行判断，8.0及以上需要设置好
       // “channelId”（没有特殊要求、唯一即可）、
       // “channelName”（用户看得到的信息）、
       // “importance”（重要等级）这三个重要参数，
       // 然后创建到NotificationManager
       private fun getNotification() : Notification? {
           return NotificationCompat.Builder(this@AudioService,"default")
               .setTicker("正在播放歌曲${list?.get(position!!)?.display_name}")
               .setSmallIcon(R.mipmap.ic_launcher)
               .setCustomContentView(getRemoteViews())
               .setWhen(System.currentTimeMillis())
               .setOngoing(true)//设置不能滑动删除通知
               .setContentIntent(getPendingIntent())//通知栏主体点击事件
               .build()
       }

       private fun getRemoteViews() : RemoteViews? {
           val remoteViews = RemoteViews(packageName, R.layout.notification)
           //修改标题和内容
           remoteViews.setTextViewText(R.id.title,list?.get(position!!)?.display_name)
           remoteViews.setTextViewText(R.id.artist,list?.get(position!!)?.artist)
           //处理上一曲、下一曲状态点击事件
           remoteViews.setOnClickPendingIntent(R.id.pre,getPrePendingIntent())
           remoteViews.setOnClickPendingIntent(R.id.state,getStatePendingIntent())
           remoteViews.setOnClickPendingIntent(R.id.next,getNextPendingIntent())
           return remoteViews
       }


       private fun getPrePendingIntent() : PendingIntent? {
           val intent = Intent(this@AudioService, AudioService::class.java)
           intent.putExtra("from", FROM_PRE)
           return PendingIntent.getService(this@AudioService,
               1,
               intent,
               PendingIntent.FLAG_IMMUTABLE)
       }


       private fun getNextPendingIntent() : PendingIntent? {
           val intent = Intent(this@AudioService, AudioService::class.java)
           intent.putExtra("from", FROM_NEXT)
           return PendingIntent.getService(this@AudioService,
               2,
               intent,
               PendingIntent.FLAG_IMMUTABLE)
       }


       private fun getStatePendingIntent() : PendingIntent? {
           val intent = Intent(this@AudioService, AudioService::class.java)
           intent.putExtra("from", FROM_STATE)
           return PendingIntent.getService(this@AudioService,
               3,
               intent,
               PendingIntent.FLAG_IMMUTABLE)
       }

       //通知栏点击事件,这里需要启动两个界面，如果只启动AudioPlayerActivity，那么退出该界面后就直接退出app了，
       // 这不符合一般应用的使用情况

       private fun getPendingIntent() : PendingIntent? {
           val intentM = Intent(this@AudioService, MainActivity::class.java)
           val intentA = Intent(this@AudioService, AudioPlayerActivity::class.java)
           intentA.putExtra("from", FROM_CONTENT)
           val intents = arrayOf(intentM,intentA)
           return PendingIntent.getActivities(this@AudioService,
               4,
               intents,
               PendingIntent.FLAG_IMMUTABLE)
       }
   }
}