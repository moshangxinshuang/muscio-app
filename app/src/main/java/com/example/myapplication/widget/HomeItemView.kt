package com.example.myapplication.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.example.myapplication.R
import com.example.myapplication.model.HomeItemBean
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_history.view.*
import kotlinx.android.synthetic.main.item_history.view.tv_title
import kotlinx.android.synthetic.main.item_home.view.*
import kotlinx.android.synthetic.main.item_history.view.img_bg as img_bg1

class HomeItemView:RelativeLayout {//覆写构造
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
    init {
        View.inflate(context, R.layout.item_home,this)
    }
    fun setData(data: HomeItemBean) {
        //歌曲名称
        tv_title.setText(data.name)
        //简介
        tv_desc.setText(data.artistName)
        //背景图片
        Picasso.get().load(data.cover).into(img_bg)
        //Picasso.get().load(data.cover+"?param=100y100").transform()
    }
}
