package com.example.myapplication.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.example.myapplication.R
import com.example.myapplication.model.HistoryItemItemBean
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryItemView:RelativeLayout {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
    init {
        View.inflate(context, R.layout.item_history, this)
    }

    fun setData(data : HistoryItemItemBean) {
        //歌单名称
        tv_title.text = data.name
        //作者
        tv_author_name.text = data.artistName
        //播放次数
        tv_publishtime.text = data.playCount.toString()
        //背景
        Picasso.get().load(data.cover).into(img_bg)
        //创作者头像
        Picasso.get().load(data.cover+"?param=100y100").transform(CropCircleTransformation())
            .into(img_author_image)
    }
}