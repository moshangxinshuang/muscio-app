package com.example.myapplication.presenter.impl

import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.base.BaseView
import com.example.myapplication.model.HomeBean
import com.example.myapplication.net.HomeRequest
import com.example.myapplication.net.ResponseHandler
import com.example.myapplication.presenter.`interface`.HomePresenter
import com.example.myapplication.view.HomeView
import org.jetbrains.anko.internals.AnkoInternals.createAnkoContext
import java.lang.ref.WeakReference

class HomePresenterimpl(val homeView :WeakReference<BaseView<HomeBean>>) : HomePresenter,ResponseHandler<HomeBean> {
    //目前代码已经精简，但是在p层强引用Homefragment会导致内存泄漏，为了解决这个问题，采用弱引用。
    override fun loadDatas() {
         HomeRequest(BaseListPresenter.TYPE_INIR_OR_REFRESH,0,this).excute()
    }
    override fun loadMoreDatas(offset : Int){
        HomeRequest(BaseListPresenter.TYPE_LOAD_MORE,offset,this).excute()
    }

    override fun onError(type:Int,msg : String?) {
        homeView.get()?.onError(msg)
    }

    override fun onSuccessed(type:Int,result : HomeBean) {
      when (type){
          BaseListPresenter.TYPE_INIR_OR_REFRESH -> homeView.get()?.loadSuccessed(result)
          BaseListPresenter.TYPE_LOAD_MORE ->homeView.get()?.onLoadMore(result)
      }
    }

    override fun destoryView() {
        //
    }
}