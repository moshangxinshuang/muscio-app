package com.example.myapplication.presenter.`interface`

import com.example.myapplication.base.BaseListPresenter

interface MvListPresenter :BaseListPresenter{
    companion object{
        val TYPE_INIR_OR_REFRESH =1
        val TYPE_LOAD_MORE =2
    }
}