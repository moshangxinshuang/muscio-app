package com.example.myapplication.presenter.impl

import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.base.BaseView
import com.example.myapplication.model.HistoryBean
import com.example.myapplication.net.HistoryRequest
import com.example.myapplication.net.ResponseHandler
import com.example.myapplication.presenter.`interface`.HistoryPresenter
import com.example.myapplication.presenter.`interface`.HomePresenter
import com.example.myapplication.ui.fragement.HistoryFragment
import java.lang.ref.WeakReference

class HistoryPresentrimpl(val historyView : WeakReference<BaseView<HistoryBean>>):HistoryPresenter,
    ResponseHandler<HistoryBean> {
    override fun loadDatas() {
        HistoryRequest(BaseListPresenter.TYPE_INIR_OR_REFRESH,0,this).excute()
    }

    override fun loadMoreDatas(offset : Int) {
        HistoryRequest(BaseListPresenter.TYPE_LOAD_MORE,offset,this).excute()
    }

    override fun onError(type : Int, msg : String?) {
        historyView.get()?.onError(msg)
    }

    override fun onSuccessed(type : Int, result : HistoryBean) {
        when(type){
            BaseListPresenter.TYPE_INIR_OR_REFRESH ->historyView.get()?. loadSuccessed(result)
            BaseListPresenter.TYPE_LOAD_MORE ->historyView.get()?.onLoadMore(result)
        }
    }

    override fun destoryView() {
        //
    }
}