package com.example.myapplication.presenter.impl

import com.example.myapplication.base.BaseListPresenter
import com.example.myapplication.model.MvPagerBean
import com.example.myapplication.net.MvListRequest
import com.example.myapplication.net.ResponseHandler
import com.example.myapplication.presenter.`interface`.MvListPresenter
import com.example.myapplication.presenter.`interface`.MvPresenter
import com.example.myapplication.view.MvListView
import kotlin.reflect.typeOf

class MvListPresenterimpl(var code:String,var mvListView: MvListView?):
    MvListPresenter,ResponseHandler<MvPagerBean>{
    override fun loadDatas() {
        MvListRequest(BaseListPresenter.TYPE_INIR_OR_REFRESH,code,0,this).excute()
    }

    override fun loadMoreDatas(offset : Int) {
        MvListRequest(BaseListPresenter.TYPE_LOAD_MORE,code,offset,this).excute()
    }

    override fun destoryView() {
        if (mvListView != null) {
            mvListView = null
        }

    }

    override fun onError(type : Int, msg : String?) {
        mvListView?.onError(msg)
    }

    override fun onSuccessed(type : Int, result : MvPagerBean) {
        if (type ==BaseListPresenter.TYPE_INIR_OR_REFRESH){
            mvListView?.loadSuccessed(result)
        }else if (type == BaseListPresenter.TYPE_LOAD_MORE){
            mvListView?.onLoadMore(result)
        }
    }
}