package com.example.myapplication.presenter.impl

import com.example.myapplication.model.MvAreaBean
import com.example.myapplication.net.MvAreaRequest
import com.example.myapplication.net.ResponseHandler
import com.example.myapplication.presenter.`interface`.MvPresenter
import com.example.myapplication.view.MvView

class MvPresenterimpl (var mvView : MvView):MvPresenter,ResponseHandler<MvAreaBean>{

    override fun onError(type : Int, msg : String?) {
        mvView.onError(msg)
    }

    override fun onSuccessed(type : Int, result : MvAreaBean) {
        mvView.onSuccess(result.tags)
    }

    override fun loadDatas() {
        MvAreaRequest(this).excute()
    }
}