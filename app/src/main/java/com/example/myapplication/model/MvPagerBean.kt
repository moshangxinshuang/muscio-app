package com.example.myapplication.model

import com.google.gson.annotations.SerializedName

data class MvPagerBean(
   @SerializedName("data") val data: List<VideosBean>
)

data class VideosBean(
    @SerializedName("artistId") val artistId: Int,
    @SerializedName("artistName") val artistName: String,
    @SerializedName("briefDesc") val briefDesc: Any,
    @SerializedName("cover") val cover: String,
    @SerializedName("desc")val desc: Any,
    @SerializedName("duration") val duration: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("mark")val mark: Int,
    @SerializedName("name")val name: String,
    @SerializedName("playCount")val playCount: Int,
    @SerializedName("subed") val subed: Boolean,
    val videoUrl: String = "http://vodkgeyttp8.vod.126.net/cloudmusic/obj/core/9337503943/e78b28855251f5c900771871537e6ae4.mp4?wsSecret=df2c6d3b9146c33d48b18840923a27f5&wsTime=1629178848"
)