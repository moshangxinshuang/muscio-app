package com.example.myapplication.model

import android.annotation.SuppressLint
import android.database.Cursor
import android.os.Parcel
import android.os.Parcelable
import android.provider.MediaStore
import com.google.gson.annotations.SerializedName

data class AudioBean (
    var data:String?,
    var size:Long,
    var display_name:String?,
    var artist:String?
):Parcelable{
    constructor(parcel : Parcel) : this(
        parcel.readString(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString()) {
    }

    override fun writeToParcel(p0 : Parcel, p1 : Int) {
        p0.writeString(data)
        p0.writeLong(size)
        p0.writeString(display_name)
        p0.writeString(artist)
    }

    override fun describeContents() : Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AudioBean> {
        override fun createFromParcel(parcel : Parcel) : AudioBean {
            return AudioBean(parcel)
        }

        override fun newArray(size : Int) : Array<AudioBean?> {
            return arrayOfNulls(size)
        }
        //在cursor位置上获取相应的bean
        @SuppressLint("Range")
        fun getAudioBean(cursor : Cursor?):AudioBean{
            val audioBean = AudioBean("",0,"","")
            cursor?.let {
                audioBean.data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                audioBean.size = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE))
                audioBean.display_name =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME))
                audioBean.display_name =
                    audioBean.display_name?.substring(0,audioBean.display_name!!.lastIndexOf("."))
                audioBean.artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
            }
            return audioBean
        }

        //根据当前位置cursor获取播放列表
        fun getAudioBeans(cursor : Cursor?) : ArrayList<AudioBean> {
            //创建集合,若cursor不为空，则解析并添加到集合中
            val list = ArrayList<AudioBean>()
            cursor?.let {
                //先将cursor移到-1位，保证集合中的元素不被漏掉
                it.moveToPosition(-1)
                //解析cursor，添加数据到集合中
                while (it.moveToNext()){
                    val audioBean = getAudioBean(it)
                    list.add(audioBean)
                }
            }
            return list
        }
    }
}