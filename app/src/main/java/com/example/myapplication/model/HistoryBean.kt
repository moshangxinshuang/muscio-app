package com.example.myapplication.model

import com.google.gson.annotations.SerializedName
import java.util.zip.DataFormatException

data class HistoryBean(
    @SerializedName("data") val songlist: List<HistoryItemItemBean>
)

data class HistoryItemItemBean (
    @SerializedName("artistId") val artistId: Int,
    @SerializedName("artistName") val artistName: String,
    @SerializedName("briefDesc") val briefDesc: Any,
    @SerializedName("cover") val cover: String,
    @SerializedName("desc")val desc: Any,
    @SerializedName("duration") val duration: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("mark")val mark: Int,
    @SerializedName("name")val name: String,
    @SerializedName("playCount")val playCount: Int,
    @SerializedName("subed") val subed: Boolean
    )


