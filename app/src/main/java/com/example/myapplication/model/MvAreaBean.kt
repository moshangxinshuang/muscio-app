package com.example.myapplication.model

import com.google.gson.annotations.SerializedName

data class MvAreaBean(
    @SerializedName("tags") val tags: List<MVTag>
)

data class MVTag(
    @SerializedName("category") val category: Int,
    @SerializedName("hot") val hot: Boolean,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("type") val type: Int
)